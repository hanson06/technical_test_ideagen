﻿using System;
using TechnicalTest.Equation;

namespace TechnicalTest
{
    class Program
    {
        private static readonly EquationExecutor EquExecutors = new EquationExecutor();
        
        static void Main()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Please Enter your Equation : (Type \"Quit\" to quit)");
                var eq = Console.ReadLine();
                if (eq.Equals( "quit", StringComparison.OrdinalIgnoreCase))
                    break;
                try
                {
                    Console.WriteLine($"{eq} = {Calculate(eq)}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Console.ReadLine();

            } while (true);
        }
        
        private static double Calculate(string sum) { 
            //Your code starts here
            return EquExecutors.Parse(sum);
        }
    }

}