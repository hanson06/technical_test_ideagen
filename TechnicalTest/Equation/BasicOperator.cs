using System.Collections.Generic;
using System.Linq;
using TechnicalTest.Equation.Operators;

namespace TechnicalTest.Equation
{
    internal static class BasicOperator
    {
        internal static string[] ListOfOperatorValue => Operators.Select(x => x.Value).ToArray();

        internal static readonly HashSet<IOperator> Operators = new HashSet<IOperator>()
        {
            new DivideOperator(),
            new MultipleOperator(),
            new AddOperator(),
            new MinusOperator(),
        };

        internal static bool IsOperator(string value)
        {
            return ListOfOperatorValue.Contains(value);
        }
    }
}