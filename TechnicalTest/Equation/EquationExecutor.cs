using System;
using System.Collections.Generic;
using System.Linq;
using TechnicalTest.Equation.Numbers;

namespace TechnicalTest.Equation
{
    public class EquationExecutor
    {
        private const string OpenBracket = "(", CloseBracket = ")";
        private readonly EquationFactory _equationFactory;
        private readonly EquationValidator _equationValidator;

        public EquationExecutor()
        {
            _equationFactory = new EquationFactory();
            _equationValidator = new EquationValidator();
        }
        
        public double Parse(string eq)
        {
            var elementList = eq.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            _equationValidator.Validate(elementList, OpenBracket, CloseBracket);
            return Calculate(elementList);
        }

        private double Calculate(string[] eq)
        {
            var numbers = Extract(eq);
            return Execute(numbers);
        }

        private List<INumber> Extract(string[] eq)
        {
            var numbers = new List<INumber>();

            for (var index = 0; index < eq.Length;)
            {
                var innerEqSize = GetInnerEquationSize(eq, index, OpenBracket, CloseBracket);
                double number;
                if (innerEqSize > 0)
                {
                    var innerEq = new string[innerEqSize-2];// remove bracket
                    Array.Copy(eq, index+1, innerEq, 0, innerEq.Length);
                    number = Calculate(innerEq);
                    index += innerEqSize;
                }
                else
                {
                    number = double.Parse(eq[index++]);
                }
                var opr = index < eq.Length ?  eq[index++] : null;
                numbers.Add(_equationFactory.CreateNumber(number, opr));
            }
            return numbers;
        }

        private double Execute(List<INumber> numbers)
        {
            var index = 0;
            while (numbers.Count > 1)
            {
                index = index < numbers.Count - 1 ? index : 0;

                if (numbers[index + 1].Merge(numbers[index]))
                    numbers.Remove(numbers[index]);
                
                index++;
            }
            return numbers.FirstOrDefault()?.Value ?? 0;
        }

        private int GetInnerEquationSize(string[] equation, int offset, string openBracket, string closeBracket)
        {
            if (equation[offset] != OpenBracket)
                return -1;

            var endIndex = offset + 1;
            var nextOpenBracket = offset + 1;

            do
            {
                endIndex = Array.IndexOf(equation, closeBracket, endIndex + 1);
                nextOpenBracket = Array.IndexOf(equation, openBracket, nextOpenBracket + 1);
            } while (nextOpenBracket > 0 && nextOpenBracket < endIndex);

            return endIndex - offset + 1;
        }
        
    }
}