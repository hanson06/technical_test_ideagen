namespace TechnicalTest.Equation.Operators
{
    public class MinusOperator : IOperator
    {
        public string Value => "-";
        public int Priority => 1;

        public double Execute(double number1, double number2) => number1 - number2;
    }
}