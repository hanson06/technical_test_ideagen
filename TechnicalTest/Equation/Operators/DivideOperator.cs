namespace TechnicalTest.Equation.Operators
{
    public class DivideOperator : IOperator
    {
        public string Value => "/";
        public int Priority => 2;
        public double Execute(double number1, double number2) => number1 / number2;
    }
}