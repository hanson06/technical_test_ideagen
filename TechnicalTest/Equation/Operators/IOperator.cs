using System;

namespace TechnicalTest.Equation.Operators
{
    public interface IOperator
    {
        string Value { get;  }

        int Priority { get;  }

        Double Execute(Double number1, Double number2);
    }
}