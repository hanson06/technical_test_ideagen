using System;
using System.Linq;

namespace TechnicalTest.Equation
{
    public class EquationValidator
    {
        public void Validate(string[] eq, string openBracket, string closeBracket)
        {
            ValidateBracketMismatch(eq, openBracket, closeBracket);

            for (var index = 0; index < eq.Length; index++)
            {
                ValidateElement(eq[index], openBracket, closeBracket);
                ValidateNextElement(eq, index, openBracket, closeBracket);
            }
        }

        private void ValidateBracketMismatch(string[] eq, string openBracket, string closeBracket)
        {
            var numOfOpenBracket = eq.Count(value => value.Equals(openBracket));
            var numOfCloseBracket = eq.Count(value => value.Equals(closeBracket));

            if (numOfOpenBracket != numOfCloseBracket)
            {
                throw new FormatException("There are a missing Open or Closing bracket");
            }
        }

        private void ValidateElement(string element, string openBracket, string closeBracket)
        {
            var isValid = element == openBracket
                          || element == closeBracket
                          || BasicOperator.ListOfOperatorValue.Contains(element)
                          || double.TryParse(element, out _);

            if (!isValid)
                throw new FormatException($"There are a invalid element or missing space near <{element}>");
        }

        private void ValidateNextElement(string[] eq, int index, string openBracket, string closeBracket)
        {
            var next = index < eq.Length - 1 ? eq[index+1] : null;
            var isFirstElement = index == 0;
            if (isFirstElement && BasicOperator.ListOfOperatorValue.Contains(eq[index]))
                throw new FormatException("Cannot start with Operator");

            if (eq[index] == openBracket)
            {
                if (BasicOperator.IsOperator(next))
                    throw new FormatException($"Operator is not allowed after <{openBracket}>");
                if (next == closeBracket)
                    throw new FormatException($"Empty bracket is not allowed");
            }
            else if (eq[index] == closeBracket && next != null)
            {
                if (double.TryParse(next, out _))
                    throw new FormatException($"Number is not allow after <{closeBracket}>");
            }
            else if (BasicOperator.IsOperator(eq[index]))
            {
                if (next == null || BasicOperator.IsOperator(next) || next == closeBracket)
                    throw new FormatException("After Operator must be number");
            }
            else if (double.TryParse(eq[index], out _) && next != null)
            {
                if (double.TryParse(next, out _))
                    throw new FormatException("Cannot be number after number");
            }
        }
    }
}