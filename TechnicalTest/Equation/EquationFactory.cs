using System.Linq;
using TechnicalTest.Equation.Numbers;
using TechnicalTest.Equation.Operators;

namespace TechnicalTest.Equation
{
    public class EquationFactory
    {
        private IOperator CreateOperator(string value) => BasicOperator.Operators.FirstOrDefault(op => op.Value == value);
        
        private INumber CreateNumber(double value, IOperator action) => new Number(value, action);

        public INumber CreateNumber(double number, string action) =>  CreateNumber(number, CreateOperator(action) );
        
    }
}