using TechnicalTest.Equation.Operators;

namespace TechnicalTest.Equation.Numbers
{
    public class Number : INumber
    {
        public double Value { get; set; }
        public IOperator Action { get; }

        public Number(double value, IOperator action)
        {
            this.Value = value;
            this.Action = action;
        }

        public bool Merge(INumber target)
        {
            if (target.Action.Priority >= (this.Action?.Priority ?? 0))
            {
                this.Value = target.Action.Execute(target.Value, this.Value);
                return true;
            }

            return false;
        }
    }
}