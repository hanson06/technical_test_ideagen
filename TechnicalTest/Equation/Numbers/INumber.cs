using System;
using TechnicalTest.Equation.Operators;

namespace TechnicalTest.Equation.Numbers
{
    public interface INumber 
    {
        Double Value { get; set; }
        IOperator Action { get; }

        bool Merge(INumber target);
    }
}